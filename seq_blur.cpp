
#include <stdio.h>
#include <stdlib.h>

#include <iostream>
#include <fstream>
#include <string>
using namespace std;

#include <time.h>

int main(int argc, const char* argv[])
{
	int w; int h;
	ifstream inpFile;
	inpFile.open(argv[1]);
	ofstream newfile;
	std::string name = argv[1];
	std::string ndim = argv[2];
	newfile.open("seq-n" + ndim + "_" + name);
	string s;
	getline(inpFile, s); // Id
	newfile << s << "\n";
	getline(inpFile, s); // Version
	newfile << s << "\n";
	getline(inpFile, s); // Dim
	newfile << s << "\n";
	
	string ws; string hs; bool done = false;
	for (int i = 0; i < s.length(); i++)
	{
		if (s[i] == ' ')
		{
			done = true;
			continue;
		}
		if (done)
		{
			hs += s[i];
		}
		else
		{
			ws += s[i];
		}
	}
	w = stoi(ws); h = stoi(hs);

	// Max
	getline(inpFile, s); 
	newfile << s << "\n";
	int* img = new int[w*h];
	int k = 0;
	while (inpFile.good())
	{
		if (k == w*h)
			break;
		getline(inpFile, s);
		*(img+k) = stoi(s);
		k++;
	}

	clock_t tStart = clock();

	//Setup matrice
	int n = atoi(argv[2]);
	int l = (n / 2);

	int* img2 = new int[w * h];
	for (int i = 0; i < w*h; i++)
	{
		int value = *(img + i);
		int div = 1;

		int ui = i - w;
		int uic = 0;
		while (ui >= 0 && uic < l)
		{
			value += *(img + ui);
			div++;
			ui -= w;
			uic++;
		}

		int di = i + w;
		int dic = 0;
		while (di < (w * h) && dic < l)
		{
			value += *(img + di);
			div++;
			di += w;
			dic++;
		}

		int li = i - 1;
		int lic = 0;
		int uli = li;
		int ulic = l;
		int dli = li;
		int dlic = l;
		while (li >= 0 && lic < l && ((li + 1) % w) != 0)
		{
			if (ulic > 0)
			{
				for (int k = 1; k < ulic; k++)
				{
					int q = li - (w*k);
					if (q >= 0)
					{
						value += *(img + q);
						div++;
					}
				}
				ulic--;
			}
			if (dlic > 0)
			{
				for (int k = 1; k < dlic; k++)
				{
					int q = li + (w*k);
					if (q < w * h)
					{
						value += *(img + q);
						div++;
					}
				}
				dlic--;
			}
			value += *(img + li);
			div++;
			li--;
			lic++;
		}

		int ri = i + 1;
		int ric = 0;
		int uri = ri;
		int uric = l;
		int dri = ri;
		int dric = l;
		while (ri < (w * h) && ric < l && ri % w != 0)
		{
			if (uric > 0)
			{
				for (int k = 1; k < uric; k++)
				{
					int q = ri - (w*k);
					if (q >= 0)
					{
						value += *(img + q);
						div++;
					}
				}
				uric--;
			}
			if (dric > 0)
			{
				for (int k = 1; k < dric; k++)
				{
					int q = ri + (w*k);
					if (q < w * h)
					{
						value += *(img + q);
						div++;
					}
				}
				dric--;
			}
			value += *(img + ri);
			div++;
			ri++;
			ric++;
		}

		*(img2 + i) = value / div;
	}

	cout << "Blur sequenziale con mask matrix di dimensione " << n << " su " << argv[1] << " completato in " << (double)(clock() - tStart) / CLOCKS_PER_SEC << "s." << endl;

	for (int i = 0; i < w * h; i++)
	{
		newfile << std::to_string(*(img2 + i)) << "\n";
	}

	inpFile.close();
	newfile.close();

	return 0;
}